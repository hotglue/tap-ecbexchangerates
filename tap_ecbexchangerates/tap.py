"""EcbExchangeRates tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_ecbexchangerates.streams import RatesStream

STREAM_TYPES = [RatesStream]


class TapEcbExchangeRates(Tap):
    """EcbExchangeRates tap class."""

    name = "tap-ecbexchangerates"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "symbols",
            th.StringType,
            default="usd",
            description="The list of currencies",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapEcbExchangeRates.cli()
