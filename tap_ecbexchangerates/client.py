"""REST client handling, including EcbExchangeRatesStream base class."""

from io import BytesIO
from typing import Iterable, List, Optional
from zipfile import ZipFile

import requests
from memoization import cached
from pendulum import parse
from singer_sdk.streams import RESTStream


class EcbExchangeRatesStream(RESTStream):
    """EcbExchangeRates stream class."""

    url_base = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist.zip"

    @property
    def partitions(self) -> Optional[List[dict]]:
        symbols = self.extract_symbols(self.config["symbols"])
        symbols = ",".join(symbols)
        return [dict(symbols=symbols)]

    @staticmethod
    def extract_symbols(symbols):
        symbols = [s.strip().upper() for s in symbols.split(",")]
        return sorted(symbols)

    @property
    def symbols(self):
        return self.extract_symbols(self.config["symbols"])

    @cached
    def symbols_states(self):
        start_date = parse(self.config.get("start_date", "2000-01-01")).replace(
            tzinfo=None
        )
        states = {p: start_date for p in self.symbols}
        partition_states = self.stream_state.get("partitions", [])
        for symbol in states:
            for partition in partition_states:
                symbols = partition.get("context", {}).get("symbols", "")
                symbols = self.extract_symbols(symbols)
                if symbol in symbols:
                    new_state = partition.get("replication_key_value")
                    if new_state:
                        new_state = parse(new_state).replace(tzinfo=None)
                        if new_state > states[symbol]:
                            states[symbol] = new_state
        return states

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        with ZipFile(BytesIO(response.content)) as zip_file:
            rates = zip_file.read("eurofxref-hist.csv").decode()

        rates_lines = rates.split("\n")

        header = [l for l in rates_lines[0].strip().split(",") if l]

        for rate_line in rates_lines[1:]:
            #Skip lines with no data
            if not rate_line:
                continue
            out_dict = dict(base="EUR")
            rate_list = [l for l in rate_line.strip().split(",") if l]
            rate_list = [None if l == "N/A" else l for l in rate_list]
            rates = {h: r for h, r in zip(header, rate_list)}
            out_dict["date"] = parse(rates.pop("Date")).replace(tzinfo=None)
            out_dict["rates"] = rates
            yield out_dict

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        symbol_state = self.symbols_states()
        rates_dict = {}
        for symbol, value in row["rates"].items():
            if symbol in self.symbols and value:
                if symbol_state[symbol] < row["date"]:
                    rates_dict[symbol] = value
        if rates_dict:
            row["rates"] = rates_dict
            row["date"] = row["date"].strftime("%Y-%m-%d")
            return row
