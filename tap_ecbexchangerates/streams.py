"""Stream type classes for tap-ecbexchangerates."""

from singer_sdk import typing as th

from tap_ecbexchangerates.client import EcbExchangeRatesStream


class RatesStream(EcbExchangeRatesStream):
    """Define rates stream."""

    name = "rates"
    path = ""
    primary_keys = ["rates"]
    replication_key = "date"

    schema = th.PropertiesList(
        th.Property("date", th.DateType),
        th.Property("base", th.StringType),
        th.Property("rates", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()
